# Conway's Game of Life

This is an implementation of Conway's Game of Life in the Go language.  The game can be presented either in a UI using [Ebiten](https://ebiten.org/) or on the command line.

Conway's Game of Life presents as a number of pixels on a grid.  The algrorithm determining if a cell appears or does not is simple.  Imagine a `3 x 3` grid, like Tic Tac Toe.  The center cell has 8 neighbors.  By counting the number of neighbors, a cell may either activate, remain or deactivate.  

The rules are as follows:

| State       | Neighbors         | New State        |
| ----------- | ----------------- | ---------------- |
| Activated   | 2 or 3            | Remain Activated |
| Deactivated | 3                 | Activate         |
| Either      | None of the above | Deactivate       |



## Usage:

To run Conway's Game of Life in a UI window, simply type this:

​    go run main.go

### Parameters

```bash
  -b int
    	the block size (n x n) (default 4)
  -h int
    	the height of the board (default 240)
  -r	repopulate when the board slows down
  -t	show 10 generations in a mini board on the command line
  -w int
    	the width of the board (default 320)
```

Alternatively, `-t` shows the a limited number of generations as command line output.

## Programming Notes

To both speed up and simplify calculation, a 1 unit border surrounds the board.  In this way, the algorithm does not need to constantly check for squares that are out-of-bounds, saving a number of IF conditionals per calculation.  

The internal representation of the game board is in a single dimensional byte array (slice). To support the 1 unix border, the overall size of the single dimensional game board is calculated as `width+2 X height+2`.

Two methods of creating the **next** generation are provided, `Next0()` and `Next()`. The difference is that`Next0()` uses a single CPU core while `Next()` subdivides the board into vertical strips, once for each core, and runs a go routine for each slice.  On an 8 core machine, this approach can run about 3-4 times faster.  To run the benchmark yourself, try the following:

```bash
$ cd conway
$ go test -bench=.
goos: darwin
goarch: amd64
pkg: gitlab.com/gojam/conway/game
Benchmark1Cpu-8    	    1902	    626695 ns/op
BenchmarkNCpus-8   	    6526	    172772 ns/op
PASS
ok  	gitlab.com/gojam/conway/game	3.580s
```


