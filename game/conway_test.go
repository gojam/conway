package game

import (
	"testing"
)

var board []byte
var conway *Conway

func init() {
	conway = New(320, 240)
	conway.Populate(0.5)
	board = make([]byte, (conway.w+2)*(conway.h+2))
	copy(board, conway.board)
}

func Benchmark1Cpu(b *testing.B) {
	for n := 0; n < b.N; n++ {
		copy(conway.board, board)
		conway.Next0()
	}
}

func BenchmarkNCpus(b *testing.B) {
	for n := 0; n < b.N; n++ {
		copy(conway.board, board)
		conway.Next()
	}
}

func BenchmarkShift(b *testing.B) {
	cnt := 0
	for n := 0; n < b.N; n++ {
		r := n % 10
		if r >> 1 == 1 {
			cnt++
		}
	}
}

func BenchmarkAdd(b *testing.B) {
	cnt := 0
	for n := 0; n < b.N; n++ {
		r := n % 10
		if r == 2 || r == 3 {
			cnt++
		}
	}
}
