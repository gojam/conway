package game

import (
	"fmt"
	"math/rand"
	"runtime"
	"strings"
)

type Conway struct {
	board      []byte
	tmp        []byte
	row, w, h  int
	generation uint64
	changes    int
}

func New(w, h int) *Conway {
	size := (w + 2) * (h + 2)
	board := make([]byte, size)
	tmp := make([]byte, size)
	row := w + 2
	return &Conway{board, tmp, row, w, h, 0, 0}
}

func (c *Conway) Populate(percentage float64) {
	for y := 1; y <= c.h; y++ {
		for x := 1; x <= c.w; x++ {
			i := y*c.row + x
			if rand.Float64() < percentage {
				c.board[i] = 1
			} else {
				c.board[i] = 0
			}
		}
	}

	c.generation = 1
	c.changes = 1
}

func (c *Conway) Repopulate(percentage float64) {
	changes := 0
	for y := 1; y <= c.h; y++ {
		for x := 1; x <= c.w; x++ {
			i := y*c.row + x
			if rand.Float64() < percentage {
				c.board[i] = 1 - c.board[i]
				changes++
			}
		}
	}

	c.changes = changes
}

func (c *Conway) String() string {
	var b strings.Builder
	for y := 1; y <= c.h; y++ {
		for x := 1; x <= c.w; x++ {
			i := y*c.row + x
			if c.board[i] == 1 {
				fmt.Fprint(&b, "* ")
			} else {
				fmt.Fprint(&b, ". ")
			}
		}
		fmt.Fprintln(&b)
	}

	return b.String()
}

// Board returns the game board.  Note there's a 1x1 empty border around the game board
// for performance boost.
func (c *Conway) Board() []byte {
	return c.board
}

// Width returns the game board width (not the width of the actual board which is
// 2 units wider due to the 1x1 empty border).
func (c *Conway) Width() int {
	return c.w
}

// Height returns the game board height (not the height of the actual board which is
// 2 units higher due to the 1x1 empty border).
func (c *Conway) Height() int {
	return c.h
}

// BoardWidth returns the width of the board with the 1x1 empty border included.
func (c *Conway) BoardWidth() int {
	return c.w + 2
}

// BoardHeight returns the height of the board with the 1x1 empty border included.
func (c *Conway) BoardHeight() int {
	return c.h + 2
}

// Generation returns the number of generations
func (c *Conway) Generation() uint64 {
	return c.generation
}

// Changes returns the number of cells that changed during the last generation
func (c *Conway) Changes() int {
	return c.changes
}

// Next0 calculates the next generation, using just 1 core.
// NOTE: Calling Next() is equivalent, but that subdivides the work
// based on the number of CPUs so should run considerably faster.
func (c *Conway) Next0() {
	c.changes = c.nextGeneration(0, 0, c.w, c.h)
	c.generation++
	c.board, c.tmp = c.tmp, c.board
}

// Next subdivides rectangle based on number of CPU cores to calculate
// next generation.
func (c *Conway) Next() {
	changes := 0
	widths := c.subWidths()
	ch := make(chan int)
	x := 0

	for i := 0; i < len(widths); i++ {
		go func(x, w int) {
			ch <- c.nextGeneration(x, 0, w, c.h)
		}(x, x+widths[i])

		x += widths[i]
	}

	for i := 0; i < len(widths); i++ {
		changes += <-ch
	}
	close(ch)

	c.generation++
	c.changes = changes
	c.board, c.tmp = c.tmp, c.board
}

// nextGeneration calculates the next state of a slice of the board.
func (c *Conway) nextGeneration(x, y, w, h int) int {
	b := c.board
	changes := 0
	for y := y + 1; y <= h; y++ {
		for x := x + 1; x <= w; x++ {
			i := y*c.row + x
			above := i - c.row
			below := i + c.row
			var cnt byte = b[above-1] + b[above] + b[above+1] + b[i-1] + b[i+1] + b[below-1] + b[below] + b[below+1]

			if cnt == 3 && b[i] == 0 {
				c.tmp[i] = 1 // activate
			} else if (cnt >> 1 == 1) && b[i] == 1 { // cnt 2 or 3
				c.tmp[i] = 1 // remain active
			} else {
				c.tmp[i] = 0 // deactivate
			}

			if b[i] != c.tmp[i] {
				changes++
			}
		}
	}

	return changes
}

// subWidths slices game board into roughly equivalent widths,
// based on the number of cpus.
func (c *Conway) subWidths() []int {
	numCpus := runtime.NumCPU()
	widths := make([]int, numCpus)
	for i := 0; i < len(widths); i++ {
		widths[i] = c.w / numCpus
	}
	for i := 0; i < c.w%numCpus; i++ {
		widths[i]++
	}
	return widths
}
