package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/hajimehoshi/ebiten"
	"gitlab.com/gojam/conway/game"
)

type Screen struct {
	game           *game.Conway
	pixels         []byte
	repopulate     bool
	lastRepopulate uint64
}

func NewScreen(g *game.Conway, blockSize int, repopulate bool) *Screen {
	pixels := make([]byte, g.Width()*g.Height()*4)

	s := &Screen{game: g, pixels: pixels, repopulate: repopulate, lastRepopulate: 0}

	ebiten.SetWindowSize(g.Width()*blockSize, g.Height()*blockSize)
	ebiten.SetWindowTitle("Conway's Game of Life")

	return s
}

func (s *Screen) Update(screen *ebiten.Image) error {

	if s.repopulate {
		size := s.game.Width() * s.game.Height()
		chg := s.game.Changes()
		gen := s.game.Generation()
		pc := float32(chg) / float32(size)

		// at 60 frames per sec, trigger at most every 30 seconds
		// when there's only about 1% change
		if pc < 0.01 && gen-s.lastRepopulate >= 1800 {
			s.lastRepopulate = gen
			fmt.Printf("Repopulate at %0.2f%%, %d changes at generation %d\n", pc*100, s.game.Changes(), s.game.Generation())
			s.game.Repopulate(.04)
		}
	}

	s.game.Next()
	return nil
}

func (s *Screen) Draw(screen *ebiten.Image) {
	pi := 0 // index into RGBA array
	gi := 0 // index into game board
	board := s.game.Board()
	for y := 1; y <= s.game.Height(); y++ {
		for x := 1; x <= s.game.Width(); x++ {
			gi = y*(s.game.BoardWidth()) + x
			v := board[gi] * 255
			ci := pi * 4
			s.pixels[ci] = 0   // RED
			s.pixels[ci+1] = v // GREEN
			s.pixels[ci+2] = 0 // BLUE
			s.pixels[ci+3] = v // ALPHA

			pi++
		}
	}

	_ = screen.ReplacePixels(s.pixels)
}

func (s *Screen) Layout(outsideWidth, outsideHeight int) (int, int) {
	return s.game.Width(), s.game.Height()
}

func main() {
	rand.Seed(time.Now().UnixNano())
	flag.Usage = func() {
		fmt.Fprintln(os.Stderr, "Run Conway's Game of Life")

		flag.PrintDefaults()
	}

	widthPtr := flag.Int("w", 320, "the width of the board")
	heightPtr := flag.Int("h", 240, "the height of the board")
	blockPtr := flag.Int("b", 4, "the block size (n x n)")
	textPtr := flag.Bool("t", false, "show 10 generations in a mini board on the command line")
	repopPtr := flag.Bool("r", false, "repopulate when the board slows down")
	flag.Parse()

	if *textPtr {
		textUI()
	} else {
		screenUI(*widthPtr, *heightPtr, *blockPtr, *repopPtr)
	}
}

func screenUI(width, height, block int, repopulate bool) {
	cg := game.New(width, height)
	cg.Populate(0.5)
	screen := NewScreen(cg, block, repopulate)
	if err := ebiten.RunGame(screen); err != nil {
		panic(err)
	}
}

func textUI() {
	cg := game.New(8, 5)
	cg.Populate(0.5)
	fmt.Println(cg)

	for i := 0; i < 10; i++ {
		cg.Next()
		fmt.Println(cg)
	}
}
